var canvas = document.getElementById("canvas");
var context= canvas.getContext ("2d");
var dataURL = false;
const clear = document.querySelector('.clear');
const undo = document.querySelector('.undo');
var canvas_height = canvas.height;
var canvas_width = canvas.width;
var cantext=false;
var radius =10;
var drawing = false;
var end=false;
var dataURL=false;
var x = 0;
var y = 0;
var prev_x = 0;
var prev_y = 0;
var todo = "pencil";
var start_x=0;
var start_y=0;
canvas.width = window.innerWidth;
canvas.height= window.innerHeight;

context.lineWidth = radius*2;

var putPoint = function(e){
    prev_x = x;
    prev_y = y;
    x = e.clientX - canvas.offsetLeft;
    y = e.clientY - canvas.offsetTop;
    if(drawing){
        if(todo=="pencil"){
            context.lineTo(e.clientX, e.clientY);
            context.stroke();
            context.beginPath();
            context.arc(e.clientX, e.clientY, radius, 0, Math.PI*2);
            tool();
            //context.fill();
            context.beginPath();
            context.moveTo(e.clientX, e.clientY);
        }
        else if(todo=="triangle"){
            tri_draw(e);
        }
        else if(todo=="circle"){
            cir_draw(e);
        }
        else if(todo=="rectangle"){
            rec_draw(e);
        }
        else if(todo=="eraser"){
            context.lineTo(e.clientX, e.clientY);
            context.stroke();
            context.beginPath();
            context.arc(e.clientX, e.clientY, radius, 0, Math.PI*2);
            context.strokeStyle="white";
            //context.fill();
            context.beginPath();
            context.moveTo(e.clientX, e.clientY);
        }
    }
  
}

var engage = function(e){
    prev_x = x;
    prev_y = y;
    start_x=x;
    start_y=y;
    x = e.clientX - canvas.offsetLeft;
    y = e.clientY - canvas.offsetTop;

    if(cantext){
       drawing= false;
    }
    else{
        cantext = false;
        drawing= true;
        putPoint(e);
    }
    if (todo == "pencil") {
        putPoint(e);
    }
    else if(todo=="draw"){
      save();
    }
    else if(todo=="rectangle"){
      save();
    }
    else if(todo=="circle"){
      save();
    }
    else if(todo=="triangle"){
      save();
    }
    else if(todo=="text"){
      draw_text();
    }
}

var disengage = function(){
    drawing = false ;
    end=true;
    if(todo=="triangle"){
        tri_draw();
    }
    context.beginPath();
    var state = context.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);
}
clear.addEventListener('click', function(){
    context.clearRect(0, 0, canvas.width, canvas.height);
  }, false);

canvas.addEventListener("mousedown",engage);
canvas.addEventListener("mousemove" , putPoint);
canvas.addEventListener("mouseup",disengage);




function todo_list(input) {
    todo = input;
    
  }
    
var font = '30px sans-serif',
    hasInput = false;

    function test2()
    {
        hasInput = false;
        cantext = true;
        test3();
    }

    function test3(){
        if(cantext){
            canvas.onclick = function(e) {
                if (hasInput) return;
                addInput(e.clientX, e.clientY);
            }
        }
    }



function addInput(x, y) {
    
    var input = document.createElement('input');
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - 4) + 'px';
    input.style.top = (y - 4) + 'px';
    input.onkeydown = handleEnter;    
    document.body.appendChild(input);
    input.focus();
    
    hasInput = true;
}

function handleEnter(e) {
    var keyCode = e.keyCode;
    if (keyCode == 13) {
        //test1();
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        //hasInput = false;
        cantext = false;
    }
}

function drawText(txt, x, y) {
    
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = font;
    context.fillText(txt, x - 4, y - 4);
    cantext = false;
}
//Eraser

function tool(){
    document.getElementById("eraser").addEventListener('click',function(){
    context.strokeStyle="white"});
    document.getElementById("pencil").addEventListener('click',function(){
    context.strokeStyle="black";
    context.fill();
})}
//Shape


function tri_draw(e){
    console.log(end,drawing);
      if(end==true){
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
        ctx.lineTo(x,y);
        ctx.lineTo(x-100,y-50);
     
        ctx.fill();
        ctx.closePath();   
        end=false;
      }
      else if(drawing==true){
        context.beginPath();
        context.moveTo(start_x,start_y);
        context.lineTo(x,y);
        context.lineTo(x-100,y-50);
        context.fill();
        context.closePath();
        
        load();
        
      }
 
}
function cir_draw(e) {
  draw_or_not=true;
  if(drawing==true){
    //load();
    context.beginPath();
    context.arc(start_x,start_y,Math.abs(x-start_x),0,2*Math.PI);
    context.stroke();
    context.fill();
    context.closePath();
  }
  else if(end==true){
    context.beginPath();
    context.arc(e.clientX,e.clientY,Math.abs(e.clientX),0,2*Math.PI,true);
    context.stroke();
    end=false;
    context.fill();
    context.closePath();
    image_data2=context.getImageData(0,0,canvas.width,canvas.height);
  }
}

function rec_draw(e) {
    drawing = true; 
    if(drawing==true){
     //load(); 
     context.beginPath();     
     context.fillRect(start_x,start_y,x-start_x,y-start_y);
     context.closePath();
   }
 
   else {  
         console.log(x ,y ,start_x ,start_y);
         context.beginPath();
         context.fillRect(start_x,start_y,x-start_x,y-start_y);
         context.closePath();
         end=false;
        image_data2=context.getImageData(0,0,canvas.width,canvas.height);
   }
   console.log(end);
}

let state = context.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);

function stopDrawing(e){
  isDrawing = false;

  var state = context.getImageData(0, 0, canvas.width, canvas.height);
  window.history.pushState(state, null);
}


window.addEventListener('popstate', changeStep, false);

function changeStep(e){
  
  context.clearRect(0, 0, canvas.width, canvas.height);

  
  if( e.state ){
    context.putImageData(e.state, 0, 0);
  }
}

function save() {
    dataURL = canvas.toDataURL();
  }
  
  function load() {
    var imageObj = new Image();
    imageObj.onload = function () {
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.drawImage(this, 0, 0);
    };
    //imageObj.src = dataURL;
    imageObj.src=dataURL;
  }