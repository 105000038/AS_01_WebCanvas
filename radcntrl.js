var minRad = 0.5,
    maxRad = 100,
    defaultRad = 5,
    interval = 5;
    radSpan = document.getElementById("radval"),
    decRad = document.getElementById("decrad"),
    incRad = document.getElementById("incrad");

var setRadius = function(newRad){
    if(newRad<minRad)
        newRad = minRad;
    else if(newRad>maxRad)
        newRad = maxRad;
    radius = newRad;
    context.lineWidth = radius*2;

    radSpan.innerHTML= radius;
}    

decRad.addEventListener("click",function(){
    setRadius(radius-interval);
});
incRad.addEventListener("click",function(){
    setRadius(radius+interval);
});

setRadius(defaultRad);