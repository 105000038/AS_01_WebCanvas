一進入網址就可以開始畫畫，有"pencil"的cursor。
點"eraser可以切換成橡皮擦。
點"pencil"可以切換為pencil.
顏色在右上角，選擇顏色時會有動態效果。
點"A"的圖示可以在畫布的隨意處輸入文字。
點"clear"這個btn可以清除畫布。
按"上一頁"跟"下一頁"可以復原跟還原。
點"Download"這個btn可以下載圖片。
點 Upload的選取檔案可以上傳圖片。
點"Circle,Rectangle,Triangle" 的那些按鈕可以畫出不同圖案。
左上角的"Radius"可以控制畫筆粗細。
